<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnswersCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers_comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('content', 255);
            $table->date('created_at');
            $table->unsignedBigInteger('profile_id');
            $table->unsignedBigInteger('answer_id');

            $table->foreign('profile_id')->references('id')->on('profiles');
            $table->foreign('answer_id')->references('id')->on('answers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answers_comments');
    }
}

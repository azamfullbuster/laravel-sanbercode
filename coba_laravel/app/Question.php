<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = "questions";
    protected $fillable = ["title", "content", "profile_id"];

    public function profile()
    {
        return $this->belongsTo('App\Profile');
    }
}

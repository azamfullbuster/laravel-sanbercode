<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }

    public function welcome(Request $request){
        return view('welcome', ["first_name" => $request["first_name"],
        "last_name" => $request["last_name"],
        "gender" => $request["gender"]]);
    }
}

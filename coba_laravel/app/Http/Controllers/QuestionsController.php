<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class QuestionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    public function index()
    {
        // buat nampilin semua pertanyaan tanpa merhatiin id tertentu
        // $questions = Question::all();

        //buat nampilin sesuai dengan user yang aktif sekarang
        $user = Auth::user();
        $questions = $user->profile->question;
        return view('questions.index', compact('questions'));
    }

    public function show($id)
    {
        $question = Question::find($id);
        return view('questions.show', compact('question'));
    }

    public function create()
    {
        return view('questions.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:questions',
            'content' => 'required',
        ]);
        Question::create([
            "title" => $request->title,
            "content" => $request->content,
            "profile_id" => Auth::user()->profile->id
        ]);
        return redirect('/questions')->with('success', 'Question Created');
    }

    public function edit($id)
    {
        $question = Question::find($id);
        return view('questions.edit', compact('question'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
        ]);

        $question = Question::find($id);
        $question->title = $request->title;
        $question->content = $request->content;
        $question->update();
        return redirect('/questions')->with('success', 'Question Edited');
    }

    public function destroy($id)
    {
        $question = Question::find($id);
        $question->delete();
        return redirect('/questions')->with('success', 'Question Deleted');
    }
}

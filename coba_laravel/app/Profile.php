<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = "profiles";
    protected $fillable = ["full_name", "email", "img_url", "user_id"];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    //membuat relation 1 profile memiliki banyak question
    public function question()
    {
        return $this->hasMany('App\Question');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ==============TUGAS CWEB STATIS LARAVEL==================
Route::get('/home', 'HomeController@index');

Route::get('/form', 'AuthController@form');

Route::post('/welcome', 'AuthController@welcome');

// ==============TUGAS TEMPLATING LARAVEL====================
Route::get('/master', function () {
    return view('layout/master');
});

Route::get('/', function () {
    return view('contents/default-table');
});

Route::get('/data-tables', function () {
    return view('contents/data-table');
});

// ==============QUESTIONS ROUTING=========================

//view data
Route::get('/questions', 'QuestionsController@index');
//view form
Route::get('/questions/create', 'QuestionsController@create');
//post form data
Route::post('/questions', 'QuestionsController@store');
//view single data
Route::get('/questions/{id}', 'QuestionsController@show');
//view edit data
Route::get('/questions/{id}/edit', 'QuestionsController@edit');
//put edit data
Route::put('/questions/{id}', 'QuestionsController@update');
//delete data
Route::delete('/questions/{id}', 'QuestionsController@destroy');

// ==============TUGAS ELOQUENT RELATIONSHIP=========================
Route::get('user', 'UserController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

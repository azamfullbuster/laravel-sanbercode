@extends('layout.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1> Question Detail</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/questions">Questions</a></li>
                    <li class="breadcrumb-item active">Detail</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="col-md-3">
        <div class="card card-success">
            <div class="card-header">
                <h3 class="card-title">{{ $question->title }}</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                </div>
                <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                {{ $question->content }}
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>

</section>
<!-- /.content -->

@endsection

@push('styles')
<link rel="stylesheet" href="{{ asset('/adminlte/dist/css/adminlte.css')}}">
@endpush
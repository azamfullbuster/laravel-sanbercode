@extends('layout.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Edit Question</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/questions">Questions</a></li>
                    <li class="breadcrumb-item active">Edit</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Question Form</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/questions/{{$question->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="form-group">
                    <label for="title">Question Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter Question Title"
                        value="{{old('title', $question->title)}}">
                    {{-- error message --}}
                    @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="content">Question Content</label>
                    <textarea class="form-control" rows="3" id="content" name="content"
                        placeholder="Write Your Question..">{{old('content', $question->content)}}</textarea>
                    {{-- error message --}}
                    @error('content')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Edit Question</button>
            </div>
        </form>
    </div>


</section>

@endsection
@extends('layout.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Questions List</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">Questions</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    {{-- Alert Message --}}
    @if (session('success'))
    <div class="alert alert-success">
        {{ session('success')}}
    </div>
    @endif

    <!-- Default box -->
    <a href="/questions/create" class="btn btn-primary mb-2"> Create New Question </a>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th style="width: 10px">#</th>
                <th>Titles</th>
                <th>Questions</th>
                <th>Author</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($questions as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->title}}</td>
                <td>{{$value->content}}</td>
                <td>{{$value->profile->full_name}}</td>
                <td>
                    <a href="/questions/{{$value->id}}" class="btn btn-info btn-sm">Show</a>
                    <a href="/questions/{{$value->id}}/edit" class="btn btn-secondary btn-sm my-1">Edit</a>
                    <form action="/questions/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger btn-sm my-1" value="Delete">
                    </form>
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="4" align="center">No data</td>
            </tr>
            @endforelse
        </tbody>
    </table>
    <!-- /.card -->

</section>
<!-- /.content -->

@endsection
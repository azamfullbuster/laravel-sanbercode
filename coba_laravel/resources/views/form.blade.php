<!DOCTYPE html>
<html>

<head>
    <title>Sanberbook Registration Form</title>
</head>

<body>
    <div style="
	background-color: white;
	margin-right: 10px;
	margin-left: 10px;">
        <h1>Buat Account Baru !</h1>

        <h3>Sign Up Form</h3>
        <form method="POST" action="/welcome">
            @csrf
            <div>
                <label for="first_name">First Name :</label><br>
                <input type="text" id="first_name" name="first_name" style="width: 220px;">
            </div>
            <br>
            <div>
                <label for="last_name">Last Name :</label><br>
                <input type="text" id="last_name" name="last_name" style="width: 220px;">
            </div>
            <br>
            <div>
                <label for="gender">Gender :</label><br>
                <input type="radio" id="male" name="gender" value="male">
                <label for="male">Male</label><br>
                <input type="radio" id="female" name="gender" value="female">
                <label for="female">Female</label><br>
                <input type="radio" id="other" name="gender" value="other">
                <label for="other">Other</label>
            </div>
            <br>
            <div>
                <label for="nationality">Nationality :</label><br>
                <select name="nationality" id="nationality">
                    <option value="Indonesian">Indonesian</option>
                    <option value="Wakandian">Wakandian</option>
                    <option value="Kuvukilandian">Kuvukilandian</option>
                    <option value="Zimbabwean">Zimbabwean</option>
                </select>
            </div>
            <br>
            <div>
                <label for="language">Language Spoken :</label><br>
                <input type="radio" id="indo" name="language" value="Bahasa Indonesia">
                <label for="indo">Bahasa Indonesia</label><br>
                <input type="radio" id="english" name="language" value="English">
                <label for="english">English</label><br>
                <input type="radio" id="other" name="language" value="Other">
                <label for="other">Other</label>
            </div>
            <br>
            <div>
                <th>Bio</th> <br>
                <td><textarea id="bio" name="bio" rows="10" cols="30"></textarea></td>
            </div>
            <br>
            <div>
                <button type="submit">Kirim</button>
            </div>
        </form>
    </div>
    <a href="/home">Kembali Ke Landing Page</a>
</body>

</html>